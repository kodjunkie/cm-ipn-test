<?php

namespace App\Concerns;


class PayKickstart
{

    /**
     * Cancel subscription
     * @link https://support.paykickstart.com/api/#cancel-subscription
     * @param string $invoiceId
     * @param int $time
     * @throws \Exception
     */
    public static function cancel_subscription(string $invoiceId, int $time)
    {
        //Set up API path and method
        $base_url = "https://app.paykickstart.com/api/";
        $route = "subscriptions/cancel";
        $url = $base_url . $route;
        $post = true;

        //Create request data string
        $data = http_build_query([
            'auth_token' => getenv('PAYKICKSTART_API_KEY'),
            'invoice_id' => $invoiceId,
            'cancel_at' => $time,
            'fire_event' => 1
        ]);

        //Execute cURL request
        $ch = curl_init();
        if ($post) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        } else {
            $url = $url . "?" . $data;
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        $output = curl_exec($ch);
        curl_close($ch);

        $output = json_decode($output);

        if ($output['success'] != 1) {
            throw new \Exception('Unable to cancel subscription.');
        }
    }

    /**
     * Verify the IPN
     * @link https://support.paykickstart.com/instant-payment-notification-ipn/
     * @param $data
     * @return bool
     */
    public static function is_valid_ipn($data)
    {
        $paramStrArr = array();
        $paramStr = NULL;

        // Loop through all POST vars
        foreach ($data as $key => $value) {
            // Ignore the encrypted key variable
            if ($key == "verification_code") continue;
            //Ignore any empty variables
            if (!$key OR !$value) continue;
            //Add IPN values to validate to a new array
            $paramStrArr[] = (string)$value;
        }

        // Alphabetically sort IPN parameters by their key. This ensures
        // the params are in the same order as when Paykickstart
        // generated the verification code, in order to prevent
        // hash key invalidation due to POST parameter order.
        ksort($paramStrArr, SORT_STRING);

        // Implode all the values into a string, delimited by "|"
        $paramStr = implode("|", $paramStrArr);

        // Generate the hash using the imploded string and secret key
        $encKey = hash_hmac('sha1', $paramStr, getenv('PAYKICKSTART_SECRET_KEY'));

        return $encKey == $data["verification_code"];
    }

}

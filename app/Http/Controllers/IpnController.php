<?php

namespace App\Http\Controllers;


use App\Concerns\PayKickstart;
use App\Models\User;
use Carbon\Carbon;

class IpnController
{
    /**
     * Handle sales IPN event based on sample data
     * @link https://support.paykickstart.com/instant-payment-notification-ipn/
     * @param array $requestParams
     * @return string
     */
    public function sales(array $requestParams)
    {
        try {
            // Check if the IPN is valid
            if (PayKickstart::is_valid_ipn($requestParams)) {

                // Create the user only if the user doesn't exists
                $user = User::firstOrCreate(
                    ['email' => $requestParams['buyer_email']],
                    [
                        'first_name' => $requestParams['buyer_first_name'],
                        'last_name' => $requestParams['buyer_last_name'],
                    ]
                );
                $user->is_subscription_active = true;
                $user->save();

                // Create the transaction
                $user->transactions()->create([
                    'amount' => $requestParams['amount'],
                    'transaction_id' => $requestParams['transaction_id'],
                    'invoice_id' => $requestParams['invoice_id'],
                    'tracking_id' => $requestParams['tracking_id'],
                    'product_id' => $requestParams['product_id'],
                    'campaign_id' => $requestParams['campaign_id'],
                    'verification_code' => $requestParams['verification_code'],
                    'transaction_time' => $requestParams['transaction_time']
                ]);

                // Cancel subscription
                PayKickstart::cancel_subscription($requestParams['invoice_id'], Carbon::now()->timestamp);
                $user->is_subscription_active = false;
                $user->save();
            } else {
                throw new \Exception('Invalid IPN');
            }
        } catch (\Exception $exception) {
            // Log any exception that is thrown
        }
    }
}

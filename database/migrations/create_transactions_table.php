<?php

use Illuminate\Database\Capsule\Manager as Capsule;

/*
 * Transactions database schema
 * */
Capsule::schema()->create('transactions', function ($table) {
    $table->increments('id');
    $table->decimal('amount', 12, 2);
    $table->string('transaction_id')->unique();
    $table->string('invoice_id')->unique();
    $table->unsignedInteger('tracking_id')->unique();
    $table->unsignedInteger('product_id')->unique();
    $table->unsignedInteger('campaign_id')->unique();
    $table->string('verification_code');
    $table->unsignedInteger('transaction_time');
    $table->unsignedInteger('user_id');
    $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
    $table->timestamps();
});

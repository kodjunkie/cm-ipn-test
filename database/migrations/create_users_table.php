<?php

use Illuminate\Database\Capsule\Manager as Capsule;

/*
 * Users database schema
 * */
Capsule::schema()->create('users', function ($table) {
    $table->increments('id');
    $table->string('first_name');
    $table->string('last_name');
    $table->string('email')->unique();
    $table->boolean('is_subscription_active')->default(false);
    $table->timestamps();
});

<?php

require_once __DIR__ . '/../bootstrap/autoload.php';

use Illuminate\Database\Capsule\Manager as Capsule;

// Drop all tables
Capsule::schema()->dropAllTables();

// Migrate all tables
require_once __DIR__ . '/migrations/create_users_table.php';
require_once __DIR__ . '/migrations/create_transactions_table.php';

echo "Database successfully migrated. \n";

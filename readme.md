# PHP test (20 pts)
Create a class (or more classes) that accepts an IPN call from Paykickstart that : 
- Validates that IPN call
- Creates a dummy user account (from $data)
- Cancels the subscription of that user using Paykickstart API (same subscription based from $data)

##### $data & IPN docs: 
[https://support.paykickstart.com/instant-payment-notification-ipn/](https://support.paykickstart.com/instant-payment-notification-ipn/)
##### API docs: 
[https://support.paykickstart.com/api/](https://support.paykickstart.com/api/)

# Requirements
-   PHP >= 7.2.12
-   MySQL / MariaDB >= 5.7.16
-   Composer

# Installation
```bash
    git clone https://gitlab.com/Paplow/cm-ipn-test.git cm_ipn_test
    cd cm_ipn_test
    composer install
    # Update .env file accordingly
    composer run-script db:migrate 
```
IPN callback url: ``https://{your-domain}/ipn/callback``

NOTE: This code wasn't tested live as i could not meet up with PayKickstart requirements.

<?php

require_once(__DIR__ . '/bootstrap/autoload.php');

/**
 * Main entrance to application
 */

$router = new \Klein\Klein();

/*
 * Route for the landing page.
 * */
$router->get('/', function ($request, $response) {
    return 'Purposely left blank.';
});

/*
 * Handle request automatically and call appropriate controller
 * */
$router->respond(['GET', 'POST'], '/[:controller]?/[**:rest]?', function ($request, $response, $service, $app) {
    // Construct the controller
    $controller = "\\App\\Http\\Controllers\\" . ucfirst($request->controller) . 'Controller';
    // controller classes are auto loaded
    if (class_exists($controller)) {
        // Persist the controller application wide
        $app->controller = new $controller();
    }
});

/*
 * Route definition for Paykickstart IPN calls
 */
$router->post('/ipn/callback', function ($request, $response, $service, $app) {
    /*
     Sample request data from example
     $params = [
        'event'                             => 'sales',
        'mode'                              => 'live',
        'payment_processor'                 => 'stripe',
        'amount'                            => 9.99,
        'buyer_ip'                          => '196.215.215.215',
        'buyer_first_name'                  => 'Ruggero',
        'buyer_last_name'                   => 'Sandri-Boriani',
        'buyer_email'                       => 'ruggero@sandri.com',
        'vendor_first_name'                 => 'Digital',
        'vendor_last_name'                  => 'Kickstart',
        'vendor_email'                      => 'support@digitalkickstart.com',
        'billing_address_1'                 => '',
        'billing_address_2'                 => '',
        'billing_city'                      => '',
        'billing_state'                     => '',
        'billing_zip'                       => '',
        'billing_country'                   => '',
        'shipping_address_1'                => '',
        'shipping_address_2'                => '',
        'shipping_city'                     => '',
        'shipping_state'                    => '',
        'shipping_zip'                      => '',
        'shipping_country'                  => '',
        'transaction_id'                    => 'PK-TN0LNO7XWR',
        'invoice_id'                        => 'PK-PZ1WK636WR',
        'tracking_id'                       => 216,
        'transaction_time'                  => 1469014598,
        'product_id'                        => 2354,
        'product_name'                      => 'SEO Snapshot - Main',
        'campaign_id'                       => 215,
        'campaign_name'                     => 'SEO Snapshot',
        'affiliate_first_name'              => 'Bob',
        'affiliate_last_name'               => 'Jones',
        'affiliate_email'                   => 'bob@jones.com',
        'affiliate_commission_amount'       => 4.99,
        'affiliate_commission_percent'      => 50,
        'ref_affiliate_first_name'          => null,
        'ref_affiliate_last_name'           => null,
        'ref_affiliate_email'               => null,
        'ref_affiliate_commission_amount'   => null,
        'ref_affiliate_commission_percent'  => null,
        'buyer_tax_number'                  => null,
        'buyer_tax_name'                    => null,
        'tax_transaction_id'                => null,
        'tax_amount'                        => null,
        'tax_percent'                       => null,
        'custom_var1'                       => 123,
        'custom_var2'                       => 'email@user.com',
        'licenses'                          => ['HPLD-XSQW-KDW3-8HTD', 'AWDF-XADWR-HYTF-4T7B'],
        'verification_code'                 => 'e2288202ad23b877c3498a6db6214b5a417b75a4'
    ];*/

    return $app->controller->sales($request->paramsPost()->all());
});


$router->dispatch();

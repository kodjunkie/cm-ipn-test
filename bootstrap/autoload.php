<?php

/*
 * This is the app's bootstrap file
 **/

use Illuminate\Database\Capsule\Manager as Capsule;

require_once(__DIR__ .'/../vendor/autoload.php');

// Configuration to autoload .env file
$dotenv = Dotenv\Dotenv::create(__DIR__.'/../');
$dotenv->load();

// Database configuration
$capsule = new Capsule;
$capsule->addConnection([
    'driver'    => 'mysql',
    'host'      => getenv('DB_HOST'),
    'database'  => getenv('DB_NAME'),
    'username'  => getenv('DB_USERNAME'),
    'password'  => getenv('DB_PASSWORD'),
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => '',
]);
// Make this Capsule instance available globally via static methods
$capsule->setAsGlobal();
// Setup the Eloquent ORM
$capsule->bootEloquent();
